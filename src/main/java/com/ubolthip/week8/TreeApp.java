package com.ubolthip.week8;

public class TreeApp {
    private String name;
    private int x;
    private int y;

    public TreeApp(String name, int x, int y) {
        this.name = name;
        this.x = x;
        this.y = y;
    }

    public void print() {
        System.out.println(name + " x:" + x + " y:" + y);
    }
}
