package com.ubolthip.week8;

public class Map {
    private int width;
    private int height;

    public Map(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public Map() {
        this(5, 5);
    }

    public void print() {
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                System.out.print('-');
            }
            System.out.println();
        }
    }
}
